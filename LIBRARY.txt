*** Settings ***
Documentation     Test suite that demonstrates the main functionalities of I,A,S,ENQ commandsfiel
Library           T24WebDriver.py
Library           Selenium2Library    run_on_failure=Nothing

*** Test Cases ***
Prerequisites
    T24 Login    INPUTTER
    @{validationRules}=    Create List    TODAY >> p.today
    Check T24 Record    DATES    BNK    ${validationRules}
    @{validationRules}=    Create List    LOCAL.COUNTRY >> p.local.country
    Check T24 Record    COMPANY    BNK    ${validationRules}
    @{validationRules}=    Create List    CO.CODE >> co.code
    Check T24 Record    COUNTRY    ${p.local.country}    ${validationRules}
    @{validationRules}=    Create List
    Check T24 Record    SCTR.GROUP.CONDITION    999    ${validationRules}
    @{enquiryConstraints}=    Create List    NAME :EQ:= Teller
    @{validationRules}=    Create List    1 >> p.acct.off.teller
    Execute T24 Enquiry    %DEPT.ACCT.OFFICER    ${enquiryConstraints}    \    ${validationRules}
    @{enquiryConstraints}=    Create List    DESCRIPTION :EQ:= Corporate
    @{validationRules}=    Create List    1 >> p.sector.corporate
    Execute T24 Enquiry    %SECTOR    ${enquiryConstraints}    \    ${validationRules}
    @{enquiryConstraints}=    Create List    DESCRIPTION :LK:= Individ
    @{validationRules}=    Create List    1 >> p.sector.individual
    Execute T24 Enquiry    %SECTOR    ${enquiryConstraints}    \    ${validationRules}

Corporate Customer
    T24 Login    INPUTTER
    @{enquiryConstraints}=    Create List    NAME :EQ:= Teller
    @{validationRules}=    Create List    1 >> p.acct.off.teller
    Execute T24 Enquiry    %DEPT.ACCT.OFFICER    ${enquiryConstraints}    \    ${validationRules}
    @{enquiryConstraints}=    Create List    DESCRIPTION :EQ:= Corporate
    @{validationRules}=    Create List    1 >> p.sector.corporate
    Execute T24 Enquiry    %SECTOR    ${enquiryConstraints}    \    ${validationRules}
    @{testDataFields}=    Create List    NAME.1:1=#AUTO-VALUE    SHORT.NAME:1=#AUTO-VALUE    MNEMONIC=#AUTO-VALUE    ACCOUNT.OFFICER=${p.acct.off.teller}    STREET:1=LAKESHORE STREET
    ...    NATIONALITY=US    RESIDENCE=US    LANGUAGE=1    SECTOR=${p.sector.corporate}
    Create Or Amend T24 Record    CUSTOMER,CORP    >> CorpCust    ${testDataFields}    \    ${EMPTY}
    Authorize T24 Record    CUSTOMER    ${CorpCust}
    @{validationRules}=    Create List    SECTOR :EQ:= ${p.sector.corporate}
    Check T24 Record    CUSTOMER,CORP    ${CorpCust}    ${validationRules}

Individual Customer
    @{enquiryConstraints}=    Create List    DESCRIPTION :EQ:= Individual
    @{validationRules}=    Create List    1 >> p.sector.individual
    Execute T24 Enquiry    %SECTOR    ${enquiryConstraints}    \    ${validationRules}
    @{testDataFields}=    Create List    ACCOUNT.OFFICER=1    CUSTOMER.STATUS=2    FAMILY.NAME=SMITH    GIVEN.NAMES=JOHN    INDUSTRY=1000
    ...    LANGUAGE=2    NAME.1:1=#AUTO-VALUE    SHORT.NAME:1=#AUTO-VALUE    MNEMONIC=#AUTO-VALUE    SECTOR=${p.sector.individual}    TARGET=2
    ...    MARITAL.STATUS=DIVORCED    TITLE=MR    GENDER=MALE
    Create Or Amend T24 Record    CUSTOMER,INPUT    \    ${testDataFields}    \    ${EMPTY}

Add print addr to corp cust
    T24 Login    INPUTTER
    @{validationRules}=    Create List    LOCAL.COUNTRY >> p.local.country
    Check T24 Record    COMPANY    BNK    ${validationRules}
    @{validationRules}=    Create List    CO.CODE >> co.code
    Check T24 Record    COUNTRY    ${p.local.country}    ${validationRules}
    @{enquiryConstraints}=    Create List    NAME :EQ:= Teller
    @{validationRules}=    Create List    1 >> p.acct.off.teller
    Execute T24 Enquiry    %DEPT.ACCT.OFFICER    ${enquiryConstraints}    \    ${validationRules}
    @{enquiryConstraints}=    Create List    DESCRIPTION :EQ:= Corporate
    @{validationRules}=    Create List    1 >> p.sector.corporate
    Execute T24 Enquiry    %SECTOR    ${enquiryConstraints}    \    ${validationRules}
    @{testDataFields}=    Create List    NAME.1:1=#AUTO-VALUE    SHORT.NAME:1=#AUTO-VALUE    MNEMONIC=#AUTO-VALUE    ACCOUNT.OFFICER=${p.acct.off.teller}    STREET:1=LAKESHORE STREET
    ...    TOWN.COUNTRY:1=BIG TOWN    POST.CODE:1=4000    COUNTRY:1=UNITED STATES    PHONE.1:1=8375693    EMAIL.1:1=INFO@COMPANY.COM    NATIONALITY=US
    ...    RESIDENCE=US    LANGUAGE=1    SECTOR=${p.sector.corporate}
    Create Or Amend T24 Record    CUSTOMER,CORP    >> CorpCust    ${testDataFields}    \    ${EMPTY}
    Authorize T24 Record    CUSTOMER    ${CorpCust}
    @{validationRules}=    Create List    SECTOR :EQ:= ${p.sector.corporate}    NAME.1 >> c.name1    SHORT.NAME >> c.short.name    STREET >> c.street    TOWN.COUNTRY >> c.town
    ...    POST.CODE >> c.post.code    COUNTRY >> c.country    EMAIL.1 >> c.email    PHONE.1 >> c.phone
    Check T24 Record    CUSTOMER,CORP    ${CorpCust}    ${validationRules}
    @{validationRules}=    Create List    SHORT.NAME :EQ:= ${c.short.name}    NAME.1 :EQ:= ${c.name1}    STREET.ADDR :EQ:= ${c.street}    POST.CODE :EQ:= ${c.post.code}    TOWN.COUNTRY :EQ:= ${c.town}
    ...    COUNTRY :EQ:= ${c.country}    EMAIL.1 :EQ:= ${c.email}    PHONE.1 :EQ:= ${c.phone}
    Check T24 Record    DE.ADDRESS,ADD2    ${p.local.country}0010001.C-${CorpCust}.PRINT.1    ${validationRules}
    @{testDataFields}=    Create List    SHORT.NAME:1=PETER    NAME.1:1=PETER TOMPSON    STREET.ADDR:1=11 SOUTH PARK AVENUE    TOWN.COUNTRY:1=VICTORIA    COUNTRY:1=US
    ...    POST.CODE:1=58476    PHONE.1=293875    SMS.1=02384934    EMAIL.1=PETER@COMP.COM
    Create Or Amend T24 Record    DE.ADDRESS,ADD2    ${p.local.country}0010001.C-${CorpCust}.PRINT.2    ${testDataFields}    \    ${EMPTY}
